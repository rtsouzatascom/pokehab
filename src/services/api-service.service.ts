import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Api } from 'src/types/api.type';
@Injectable({
  providedIn: 'root'
})
export class ApiService {


  public apiService: Api ={
    url:''
  }
  constructor() { }

  getApi(){

    this.apiService.url = environment.urlBase;
    return this.apiService;

  }
}
