import { Injectable } from '@angular/core';
import { ApiService } from '../../services/api-service.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class SearchServiceService {
  public body;
  constructor(
    private apiService:ApiService,
    private http: HttpClient
  ) { }

  getPokemon(pokemon:any){
    this.body = new FormData();
    this.body.append('action', 'SearchService');
    this.body.append("pokemon", JSON.stringify(pokemon));


    const api = this.apiService.getApi();
    return this.http.post<any>(`${api['url']}`,this.body);
  }
}
