import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SearchServiceService } from './search-service.service';
import { finalize, switchMap, tap } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AnimationController } from '@ionic/angular';

import * as _ from 'lodash';
@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements AfterViewInit {

  @ViewChild("image1", { read:ElementRef, static:true }) image1:ElementRef;
  searchFormGroup: FormGroup;

 
  public listPokemon;
  animation1
  animation2
  busy = false;
  private fetchData$: BehaviorSubject<void> = new BehaviorSubject(null);
  public pokemon="";
  
  

  constructor( 
    private searchService:SearchServiceService,
    private formBuilder: FormBuilder,
    private animationCtrl:AnimationController) { }
 
  ngAfterViewInit(): void {

    this.animation1 = this.animationCtrl.create()
      .addElement(this.image1.nativeElement)
      .duration(1000)
      .direction('alternate')
      .iterations(Infinity)
      .keyframes([
        { offset: 0, transform: 'scale(0.7)', opacity: '1' },
        { offset: 1, transform: 'scale(1)', opacity: '1'
       }
    ]);
  //  this.searchFormGroup = this.formBuilder.group({
  //    pokemon:["", Validators.required]
  //  })

  }
  searchPokemon(){
   this.animation1.play();
   
        this.fetchData$
    .pipe(
      tap(()=>(this.busy = true)),
      switchMap(() =>
        this.searchService.getPokemon(this.pokemon.toLowerCase())
        .pipe(finalize(() => (this.busy = false)))
      )
    )
    .subscribe((data)=>{
      this.listPokemon = JSON.parse(data.obj);

      this.listPokemon.abilities = _.sortBy(this.listPokemon.abilities,"ability.name");
      this.animation1.pause();
          
      })
    
  
  }

}
