import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';
import { SearchPageRoutingModule } from './search-routing.module';

import { SearchPage } from './search.page';
import { ReactiveFormsModule} from '@angular/forms' 
import { SearchServiceService } from './search-service.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    ReactiveFormsModule,
    SearchPageRoutingModule
  ],
  providers:[SearchServiceService],
  declarations: [SearchPage]
})
export class SearchPageModule {}
